# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from portmod.pybuild import Pybuild1, InstallDir, File
from pyclass import NexusMod


class Mod(NexusMod, Pybuild1):
    NAME = "Blighted Animals Retextured"
    DESC = "Adds unique textures to all blighted animals"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/42245"
    LICENSE = "all-rights-reserved"
    RDEPEND = "base/morrowind"
    KEYWORDS = "openmw"
    SRC_URI = """
        Nix-Hound_knockout_animation_fix-42245-.7z
        texture_size_256? ( Blighted_Animals_with_Vanilla_textures-42245-1-2.7z )
        texture_size_512? ( Blighted_Animals_with_Darknuts_512_textures-42245-1-2.7z )
        texture_size_1024? (
            Blighted_Animals_with_Darknuts_1024_textures-42245-1-2.7z
        )
    """
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/42245"
    TEXTURE_SIZES = "256 512 1024"
    INSTALL_DIRS = [
        InstallDir(".", SOURCE="Nix-Hound_knockout_animation_fix-42245-.7z"),
        InstallDir(
            ".",
            PLUGINS=[File("BlightedAnimalsRetextured.esp")],
            SOURCE="Blighted_Animals_with_Vanilla_textures-42245-1-2.7z",
            REQUIRED_USE="texture_size_256",
        ),
        InstallDir(
            ".",
            PLUGINS=[File("BlightedAnimalsRetextured.esp")],
            SOURCE="Blighted_Animals_with_Darknuts_512_textures-42245-1-2.7z",
            REQUIRED_USE="texture_size_512",
        ),
        InstallDir(
            ".",
            PLUGINS=[File("BlightedAnimalsRetextured.esp")],
            SOURCE="Blighted_Animals_with_Darknuts_1024_textures-42245-1-2.7z",
            REQUIRED_USE="texture_size_1024",
        ),
    ]
